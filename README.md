# LPSE Web Scraping

Scraping website lpse menggunakan Node JS (axios dan cheerio) dan postgresql.

REQUIRED 3rd Party :
1. Axios : npm i axios
2. Cheerio : npm i cheerio
3. Postgresql : npm i pg
4. Random String Generator : npm i randomstring
5. Winston : npm i winston
6. Read Excel File : npm i read-excel-file --save

How to run app :
+ Run List LPSE (All datas)
    - Open terminal.
    - Get into "functions" directory.
    - Run node insert-excel.js.
+ Run Scrap app
    - Open terminal.
    - Get into "main" directory.
    - Run node main.js.

Log File :
1. Open "log" directory.
2. Select error.log or info.log.
    - Error Log (error.log) : File that store error parts.
    - Info Log (info.log) : File that store error and success parts.