// require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();
require('dotenv').config({
    path: '../.env'
})
const insert = require('../functions/insert-data')
const check = require('../functions/checking-done-tender')
const functions = require('../functions/functions')
const axios = require('axios')
const cheerio = require('cheerio')
const winston = require('winston')
const schedule = require('node-schedule')
const {} = require('console')
const {} = require('module')
const {
    Client
} = require('pg')

// Log Configuration
const dateNow = Date().toString()
const logger = winston.createLogger({
    level: winston.config.syslog.levels,
    format: winston.format.json(),
    transports: [
        new winston.transports.File({
            filename: '../log/error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: '../log/info.log',
            level: 'info'
        }),
    ],
})

// PostgreSQL Configuration
const client = new Client({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
})
let spend_time = 0
let collecting_interval = setInterval(() => {
    spend_time += 1
}, 1000)

client.connect()
    .then(() => {
        setTimeout(() => {
            console.log('Warning!!\nPerhaps, it spends so much time. Please wait.')
        }, 2000);
        setTimeout(() => {
            console.log('Status : Collecting all data...')
        }, 3000);
    })
setTimeout(() => {
    schedule.scheduleJob(process.env.SCHEDULE_HOUR, function () {
        execute_query()
    })
}, 4000);

async function execute_query() {
    let count_error = 1
    let count_success = 1
    let results = await client
        .query('SELECT website as url FROM list_lpse WHERE website IS NOT NULL')
        .then(async (res) => {
            let urls = res.rows
            let all_records = []

            for (let loop = 0; loop < urls.length; loop++) {
                let split = urls[loop]['url'].split('/')
                if (split.length == 4) {
                    split.pop()
                }
                let base_url = split.join('/')
                console.log('Status : Collecting data ' + (loop + 1) + ' from ' + base_url + '/eproc4')
                let content = await axios.get(base_url + '/eproc4', {
                        timeout: 300000
                    })
                    .then(async (response) => {
                        let html = response.data
                        let dom = cheerio.load(html)
                        let tender = {
                            'tender': load_json('.bs-callout-info', dom)
                        }
                        let non_tender = {
                            'non_tender': load_json('.bs-callout-danger', dom)
                        }

                        // --------------------------------- FOR TENDER --------------------------------- //
                        for (let t = 0; t < tender['tender'].length; t++) {
                            for (let c = 0; c < tender['tender'][t]['data'].length; c++) {
                                let url_tender = base_url + tender['tender'][t]['data'][c]['url']
                                let data_url = await axios.get(url_tender, {
                                        timeout: 300000
                                    })
                                    .then(async function (resp) {
                                        let html_1 = resp.data
                                        let dom_1 = cheerio.load(html_1)
                                        let arr_informasi_tender = []
                                        dom_1('.table.table-condensed.table-bordered tbody').children().each((i, elm) => {
                                            if (i < 17) {
                                                let th = dom_1(elm).find('th').remove().html()
                                                let td = dom_1(elm).html().replace(/\s\s+/g, '')
                                                arr_informasi_tender[i] = {
                                                    'head': th,
                                                    'body': td
                                                }
                                            }
                                        })
                                        for (let x = 0; x < arr_informasi_tender.length; x++) {
                                            let body = arr_informasi_tender[x]['body']
                                            if (x == 0 || x == 1) {
                                                arr_informasi_tender[x]['body'] = dom_1(body).find('strong').html()
                                            }
                                            if (x == 2) {
                                                let rup = []
                                                dom_1(body).find('td').each((i, elm) => {
                                                    let value = dom_1(elm).text()
                                                    if (i == 0) {
                                                        rup['Kode RUP'] = value
                                                    } else if (i == 1) {
                                                        rup['Nama Paket'] = value
                                                    } else if (i == 2) {
                                                        rup['Sumber Dana'] = value
                                                    }
                                                })
                                                arr_informasi_tender[x]['body'] = rup
                                            }
                                            if (x == 3 || x == 4 || x == 6 || x == 7 || x == 8 || x == 9 || x == 10 || x == 12) {
                                                arr_informasi_tender[x]['body'] = dom_1(body).text().replace(/\s\s+/g, '')
                                                if (x == 12) {
                                                    let body_1 = arr_informasi_tender[x + 1]['body']
                                                    let string_body_1 = dom_1(body_1).text()
                                                    let body_2 = arr_informasi_tender[x + 2]['body']
                                                    let string_body_2 = dom_1(body_2).text()
                                                    let temp = {
                                                        'Cara Pembayaran': dom_1(body).text().replace(/\s\s+/g, ''),
                                                        'Lokasi Pekerjaan': string_body_1,
                                                        'Kualifikasi Usaha': string_body_2,
                                                    }
                                                    arr_informasi_tender[x]['body'] = temp
                                                }
                                            }
                                            if (x == 5) {
                                                let url_tender_saat_ini = base_url + dom_1(body).find('a').attr('href')
                                                let temp = []
                                                temp['url'] = url_tender_saat_ini
                                                if (c == 0) {
                                                    let jadwal_tender = await axios.get(url_tender_saat_ini, {
                                                            timeout: 300000
                                                        })
                                                        .then((res) => {
                                                            let html_2 = res.data
                                                            let dom_2 = cheerio.load(html_2)
                                                            let jadwal = []
                                                            dom_2('table tbody').children().each((i, el) => {
                                                                if (i > 0) {
                                                                    let temp = []
                                                                    dom_2(el).children().each((x, elm) => {
                                                                        let row = dom_2(elm).text().replace(/\s\s+/g, '')
                                                                        temp.push(row)
                                                                    })
                                                                    jadwal.push(temp)
                                                                }
                                                            })

                                                            return jadwal
                                                        })
                                                        .catch(function (error) {
                                                            console.log('Tender Jadwal Error : ' + error.message)
                                                            // logger.log({
                                                            //     number: count_error,
                                                            //     level: 'error',
                                                            //     message: error.message + ' | url : ' + base_url,
                                                            //     runDate: dateNow,
                                                            // })
                                                        })
                                                    temp['jadwal'] = jadwal_tender
                                                    arr_informasi_tender[x]['body'] = temp
                                                }
                                            }
                                            if (x == 11) {
                                                let temp = dom_1(body).text().replace('Rp ', '').replace('Rp', '').split(' ')
                                                arr_informasi_tender[x]['body'] = temp
                                            }
                                            // ISI FUNGSI YANG DIKOMEN DITARUH DISINI YAAA........
                                            if (x == 16) {
                                                arr_informasi_tender[x]['body'] = dom_1(body).text()
                                            }
                                        }
                                        arr_informasi_tender.splice(13, 2)
                                        return arr_informasi_tender
                                    })
                                    .catch(function (error) {
                                        console.log('Tender Error : ' + error.message)
                                        // logger.log({
                                        //     number: count_error,
                                        //     level: 'error',
                                        //     message: error.message + ' | url : ' + base_url,
                                        //     runDate: dateNow,
                                        // })
                                    })
                                tender['tender'][t]['data'][c]['data_url'] = data_url
                            }
                        }

                        // ------------------------------- FOR NON TENDER ------------------------------- //
                        for (let n = 0; n < non_tender['non_tender'].length; n++) {
                            for (let c = 0; c < non_tender['non_tender'][n]['data'].length; c++) {
                                let url_non_tender = base_url + non_tender['non_tender'][n]['data'][c]['url']
                                let data_url = await axios.get(url_non_tender, {
                                        timeout: 300000
                                    })
                                    .then(async (resp) => {
                                        let html_1 = resp.data
                                        let dom_1 = cheerio.load(html_1)
                                        let arr_informasi_tender = []
                                        dom_1('.table.table-condensed.table-bordered tbody').children().each((i, elm) => {
                                            if (i < 17) {
                                                let th = dom_1(elm).find('th').remove().html()
                                                let td = dom_1(elm).html().replace(/\s\s+/g, '')
                                                arr_informasi_tender[i] = {
                                                    'head': th,
                                                    'body': td
                                                }
                                            }
                                        })
                                        for (let a = 0; a < arr_informasi_tender.length; a++) {
                                            let elemen = arr_informasi_tender[a]['body']
                                            if (a == 0 || a == 1 || a == 2 || a == 3 || a == 5 || a == 6 || a == 7 || a == 8 || a == 9 || a == 11 || a == 12) {
                                                arr_informasi_tender[a]['body'] = dom_1(elemen).text()
                                            }
                                            if (a == 4) {
                                                let url_tender_saat_ini = base_url + dom_1(elemen).find('a').attr('href')
                                                let temp = []
                                                temp['url'] = url_tender_saat_ini
                                                let jadwal_tender = await axios.get(url_tender_saat_ini, {
                                                        timeout: 300000
                                                    })
                                                    .then((res) => {
                                                        let html_2 = res.data
                                                        let dom_2 = cheerio.load(html_2)
                                                        let jadwal = []
                                                        dom_2('table tbody').children().each((i, el) => {
                                                            if (i > 0) {
                                                                let temp = []
                                                                dom_2(el).children().each((x, elm) => {
                                                                    let row = dom_2(elm).text().replace(/\s\s+/g, '')
                                                                    temp.push(row)
                                                                })
                                                                jadwal.push(temp)
                                                            }
                                                        })
                                                        return jadwal
                                                    })
                                                    .catch(function (error) {
                                                        console.log('Non Tender Jadwal Error : ' + error.message)
                                                        // logger.log({
                                                        //     number: count_error,
                                                        //     level: 'error',
                                                        //     message: error.message + ' | url : ' + base_url,
                                                        //     runDate: dateNow,
                                                        // })
                                                    })
                                                temp['jadwal'] = jadwal_tender
                                                arr_informasi_tender[a]['body'] = temp
                                            }
                                            if (a == 10) {
                                                let temp = dom_1(elemen).text().replace('Rp ', '').replace('Rp', '').split(' ')
                                                arr_informasi_tender[a]['body'] = temp
                                            }
                                        }
                                        return arr_informasi_tender
                                    })
                                    .catch(function (error) {
                                        console.log(count_error + ' Error : ' + error.message)
                                        // logger.log({
                                        //     number: count_error,
                                        //     level: 'error',
                                        //     message: error.message + ' | url : ' + base_url,
                                        //     runDate: dateNow,
                                        // })
                                    })
                                non_tender['non_tender'][n]['data'][c]['data_url'] = data_url
                            }
                        }

                        function load_json(kelas, $) {
                            let arr_class_name = []
                            let index_array = 0
                            $(kelas).each((i, el) => {
                                let badge_value = $(el).find('.badge').text()
                                if (badge_value > 0) {
                                    let onclick = $(el).find('a').attr('onclick')
                                    let nama_paket = onclick.replace("$('.", "")
                                    let data = []
                                    nama_paket = nama_paket.replace("').toggle();", "")
                                    arr_class_name[index_array] = {
                                        nama_paket,
                                        data
                                    }
                                    index_array++
                                }
                            })
                            for (let index = 0; index < arr_class_name.length; index++) {
                                let class_name = arr_class_name[index]['nama_paket']
                                $('tr[class=' + class_name + ']').each((i, el) => {
                                    let url = $(el).find('td a[target=_blank]').attr('href')
                                    let judul = $(el).find('a[target=_blank]').text()
                                    let hps = $(el).find('td[class=table-hps]').text()
                                    let akhir_pendaftaran = $(el).find('td[class=center]').text()
                                    let data = {
                                        judul,
                                        hps,
                                        akhir_pendaftaran,
                                        url,
                                    }
                                    arr_class_name[index]['data'].push(data)
                                })
                            }
                            return arr_class_name
                        }

                        let json_data = []
                        json_data['url'] = base_url
                        json_data['tender'] = tender['tender']
                        json_data['non_tender'] = non_tender['non_tender']
                        return json_data;
                    })
                    .catch(function (error) {
                        console.log(count_error + '. Error : ' + error.message)
                        logger.log({
                            number: count_error,
                            level: 'error',
                            message: error.message + ' | url : ' + base_url,
                            runDate: dateNow,
                        })
                        count_error++
                    })
                if (typeof content != 'undefined') {
                    all_records[loop] = content
                    logger.log({
                        number: count_success,
                        level: 'info',
                        message: 'Successfully collected data from ' + base_url,
                        runDate: dateNow,
                    })
                    count_success++
                }
            }
            return all_records
        })
        .finally(() => {
            client.end()
        })

    clearInterval(collecting_interval)
    console.log('Status : Collecting data took ' + functions.time_conversion(spend_time))
    logger.log({
        number: 999999999,
        level: 'info',
        message: 'Status : Collecting data took ' + functions.time_conversion(spend_time),
        runDate: dateNow,
    })
    setTimeout(() => {
        console.log('Please, wait. App is trying to update your data...')
        insert.insert(results)
    }, 5000);
    setTimeout(() => {
        console.log('Please, wait. App is trying to check all of your data...')
        check.check()
    }, 5000);
}

// module.exports.run = execute_query
/**
    function fungsi_yang_dikomen() {
        if (x == 15) {
            let syarat_kualifikasi = []
            syarat_kualifikasi['administrasi'] = []
            syarat_kualifikasi['teknis'] = []
            syarat_kualifikasi['keuangan'] = []
            dom_1(body).find('h5').each((i, elm) => {
                let head_obj = dom_1(elm).html()
                let judul = []
                judul['judul'] = head_obj
                if (i == 0) {
                    syarat_kualifikasi['administrasi'].push(judul)
                }
                if (i == 1) {
                    syarat_kualifikasi['teknis'].push(judul)
                }
                if (i == 2) {
                    syarat_kualifikasi['keuangan'].push(judul)
                }
            })
            dom_1(body).find('table').each((i, element) => {
                if (i == 0) {
                    let administrasi = []
                    let temp = []
                    dom_1(element).find('tbody').find('tr').each((a, elm) => {
                        let value = dom_1(elm).text()
                        if (a == 1) {
                            let obj_izin_usaha = []
                            dom_1(elm).find('tbody').find('tr').children().each((x, el) => {
                                let value_izin_usaha = dom_1(el).text()
                                if (x == 1) {
                                    obj_izin_usaha['jenis_izin'] = value_izin_usaha
                                }
                                if (x == 3) {
                                    obj_izin_usaha['iujk'] = value_izin_usaha
                                }
                                if (x == 5) {
                                    obj_izin_usaha['sbu'] = value_izin_usaha
                                }
                                if (x == 7) {
                                    obj_izin_usaha['iup'] = value_izin_usaha
                                }
                            })
                            administrasi['izin_usaha'] = obj_izin_usaha
                        }
                        if (a >= 6) {
                            temp.push(value)
                            administrasi['lainnya'] = temp
                        }
                    })
                    syarat_kualifikasi['administrasi'].push(administrasi)
                }
                if (i == 6) {
                    console.log(dom_1(element).find('td').text())
                    console.log('----------------------------------------------- ' + i)
                }
            })
        }
    }
 **/