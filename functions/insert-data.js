const functions = require('./functions')
var rs = require('randomstring')
const {
    Client
} = require('pg')
const {
    disconnect
} = require('process')
const client = new Client({
    user: 'postgres',
    host: '127.0.0.1',
    database: 'lpse-web-scraping',
    password: 'firmantria10',
    post: 5432,
})
client.connect()

async function insert(arr) {
    let param_end_connection = 0
    let spend_time = 0
    let inserting_interval = setInterval(() => {
        spend_time += 1
    }, 1000)
    let insert_data_loop_non_tender = 0
    let update_data_loop_non_tender = 0
    let insert_data_loop_tender = 0
    let update_data_loop_tender = 0

    if (arr.length != 0) {
        for (let a = 0; a < arr.length; a++) {
            let tender = arr[a]
            if (typeof tender != 'undefined') {
                let url = tender['url']
                tender = tender['tender']
                // TENDER
                if (tender.length != 0) {
                    for (let x = 0; x < tender.length; x++) {
                        let data = tender[x]['data']
                        for (let a = 0; a < data.length; a++) {
                            let judul = ''
                            let hps = ''
                            let akhir_pendaftaran = ''
                            let url_pengumuman_lelang = ''
                            if (typeof tender[x]['data'][a] !== 'undefined') {
                                judul = regex(tender[x]['data'][a]['judul'])
                                hps = regex(tender[x]['data'][a]['hps'])
                                akhir_pendaftaran = regex(tender[x]['data'][a]['akhir_pendaftaran'])
                                url_pengumuman_lelang = tender[x]['data'][a]['url']
                                data_url = tender[x]['data'][a]['data_url']

                                let id_lpse = await client
                                    .query("SELECT id FROM list_lpse WHERE website like '%" + url + "%'")
                                    .then(res => {
                                        return res.rows
                                    })
                                    .catch(e => console.error(e.stack))
                                if (id_lpse.length !== 0 || id_lpse !== null) {
                                    for (let i = 0; i < id_lpse.length; i++) {
                                        // generate random string
                                        let uniq_string = rs.generate(50)
                                        let uniq_string_informasi_tender = rs.generate(50)
                                        let uniq_string_rup = rs.generate(50)
                                        let uniq_string_jenis_kontrak = rs.generate(50)

                                        let id = id_lpse[i]['id']
                                        let is_exist = false;
                                        is_exist = await client
                                            .query(`SELECT count(*) is_exist FROM eproc4 WHERE judul='${judul}' AND hps='${hps}' AND akhir_pendaftaran='${akhir_pendaftaran}' AND id_lpse='${id}'`)
                                            .then((res) => {
                                                if (res.rows[0]['is_exist'] === '0') {
                                                    return false
                                                } else {
                                                    return true
                                                }
                                            })
                                        if (is_exist) {
                                            update_data_loop_tender++
                                            console.log('Status : Updating data ' + update_data_loop_tender + ' | Tender')
                                            let uniq_string_eproc4 = await client
                                                .query(`SELECT uniq_string FROM eproc4 WHERE judul='${judul}' AND hps='${hps}' AND akhir_pendaftaran='${akhir_pendaftaran}' AND url='${url_pengumuman_lelang}' AND id_lpse='${id}'`)
                                                .then((res) => {
                                                    if (res.rows.length === 0) {
                                                        return 0
                                                    } else {
                                                        return res.rows[0]['uniq_string']
                                                    }
                                                })
                                            if (uniq_string_eproc4 !== 0) {
                                                let sql = "UPDATE eproc4 SET judul='" + judul + "', hps='" + hps + "', akhir_pendaftaran='" + akhir_pendaftaran + "', url='" + url_pengumuman_lelang + "', updatedat=now() WHERE id_lpse=" + id + " AND uniq_string='" + uniq_string_eproc4 + "'"
                                                param_end_connection += await client
                                                    .query(sql)
                                                    .then(async () => {
                                                        let param_update = 0
                                                        let id_eproc4 = await client
                                                            .query(`SELECT id FROM eproc4 WHERE uniq_string=$1`, [uniq_string_eproc4])
                                                            .then((res) => {
                                                                return res.rows[0]['id']
                                                            })
                                                        // start param - jenis kontrak
                                                        let cara_pembayaran = ''
                                                        let lokasi_pekerjaan = ''
                                                        let kualifikasi_usaha = ''
                                                        if (typeof data_url[12] !== 'undefined') {
                                                            cara_pembayaran = regex(data_url[12]['body']['Cara Pembayaran'])
                                                            lokasi_pekerjaan = regex(data_url[12]['body']['Lokasi Pekerjaan'])
                                                            kualifikasi_usaha = regex(data_url[12]['body']['Kualifikasi Usaha'])
                                                        }
                                                        // end param - jenis kontrak
                                                        let sql_jenis_kontrak = "SELECT id FROM jenis_kontrak WHERE cara_pembayaran=$1 AND lokasi_pekerjaan=$2 AND kualifikasi_usaha=$3"
                                                        let arr_jenis_kontrak = [
                                                            cara_pembayaran,
                                                            lokasi_pekerjaan,
                                                            kualifikasi_usaha
                                                        ]
                                                        let cek_jenis_kontrak = await client.query(sql_jenis_kontrak, arr_jenis_kontrak)
                                                            .then(async (res) => {
                                                                if (res.rows.length === 0) {
                                                                    let sql_insert_jenis_kontrak = `INSERT INTO jenis_kontrak(cara_pembayaran, lokasi_pekerjaan, kualifikasi_usaha, uniq_string, createdat, updatedat) VALUES ('${cara_pembayaran}', '${lokasi_pekerjaan}', '${kualifikasi_usaha}', '${uniq_string_jenis_kontrak}', now(), now())`
                                                                    let insert_jenis_kontrak = await client.query(sql_insert_jenis_kontrak)
                                                                        .then(() => {
                                                                            return 'berhasil'
                                                                        })
                                                                        .catch((e) => {
                                                                            return 'gagal'
                                                                        })
                                                                    return insert_jenis_kontrak
                                                                } else {
                                                                    let id = res.rows[0]['id']
                                                                    let sql_update_jenis_kontrak = `UPDATE jenis_kontrak SET cara_pembayaran='${cara_pembayaran}', lokasi_pekerjaan='${lokasi_pekerjaan}', kualifikasi_usaha='${kualifikasi_usaha}', uniq_string='${uniq_string_jenis_kontrak}', updatedat=now() WHERE id=${id}`
                                                                    let update_jenis_kontrak = await client.query(sql_update_jenis_kontrak)
                                                                        .then(() => {
                                                                            return 'berhasil'
                                                                        })
                                                                        .catch((e) => {
                                                                            return 'gagal'
                                                                        })
                                                                    return update_jenis_kontrak
                                                                }
                                                            })
                                                        if (cek_jenis_kontrak == 'berhasil') {
                                                            let sql_get_id_jenis_kontrak = `SELECT id FROM jenis_kontrak WHERE uniq_string='${uniq_string_jenis_kontrak}'`
                                                            let id_jenis_kontrak = await client.query(sql_get_id_jenis_kontrak)
                                                                .then((r) => {
                                                                    return r.rows[0]['id']
                                                                })
                                                                .catch((e) => {
                                                                    return 0
                                                                })
                                                            if (id_jenis_kontrak !== 0) {
                                                                let kode_rup = ''
                                                                let nama_paket = ''
                                                                let sumber_dana = ''
                                                                if (typeof data_url[2]['body'] !== 'undefined') {
                                                                    if (typeof data_url[2]['body']['Kode RUP'] !== 'undefined') {
                                                                        kode_rup = regex(data_url[2]['body']['Kode RUP'])
                                                                    }
                                                                    if (typeof data_url[2]['body']['Nama Paket'] !== 'undefined') {
                                                                        kode_rup = regex(data_url[2]['body']['Nama Paket'])
                                                                    }
                                                                    if (typeof data_url[2]['body']['Sumber Dana'] !== 'undefined') {
                                                                        kode_rup = regex(data_url[2]['body']['Sumber Dana'])
                                                                    }
                                                                }
                                                                let id_rup = await client.query('SELECT id FROM rup WHERE kode_rup=$1 AND nama_paket=$2 AND sumber_dana=$3', [kode_rup, nama_paket, sumber_dana])
                                                                    .then(async (res) => {
                                                                        if (res.rows.length !== 0) {
                                                                            // update
                                                                            let id_rup = res.rows[0]['id']
                                                                            return await client.query(`UPDATE rup SET kode_rup='${kode_rup}', nama_paket='${nama_paket}', sumber_dana='${sumber_dana}', updatedat=now() WHERE id=${id_rup}`)
                                                                                .then(() => {
                                                                                    let id_rup_update = id_rup
                                                                                    return id_rup_update
                                                                                })
                                                                                .catch(async () => {
                                                                                    let sql = "INSERT INTO rup(kode_rup, nama_paket, sumber_dana, uniq_string, createdat, updatedat) VALUES('" + kode_rup + "', '" + nama_paket + "', '" + sumber_dana + "', '" + uniq_string_rup + "', now(), now())"
                                                                                    let id_rup_insert = await client.query(sql)
                                                                                        .then(async () => {
                                                                                            let sql = "SELECT id FROM rup WHERE uniq_string=$1"
                                                                                            let id = await client.query(sql, [uniq_string_rup])
                                                                                                .then((res) => {
                                                                                                    return res.rows[0]['id']
                                                                                                })
                                                                                            return id
                                                                                        })
                                                                                    return id_rup_insert
                                                                                })
                                                                        } else {
                                                                            // insert
                                                                            let sql = "INSERT INTO rup(kode_rup, nama_paket, sumber_dana, uniq_string, createdat, updatedat) VALUES('" + kode_rup + "', '" + nama_paket + "', '" + sumber_dana + "', '" + uniq_string_rup + "', now(), now())"
                                                                            let id_rup_insert = await client.query(sql)
                                                                                .then(async () => {
                                                                                    let sql = "SELECT id FROM rup WHERE uniq_string=$1"
                                                                                    let id = await client.query(sql, [uniq_string_rup])
                                                                                        .then((res) => {
                                                                                            return res.rows[0]['id']
                                                                                        })
                                                                                    return id
                                                                                })
                                                                            return id_rup_insert
                                                                        }
                                                                    })

                                                                let kode_tender = ''
                                                                if (typeof data_url[0] !== 'undefined') {
                                                                    kode_tender = regex(data_url[0]['body'])
                                                                }
                                                                let nama_tender = ''
                                                                if (typeof data_url[1] !== 'undefined') {
                                                                    nama_tender = regex(data_url[1]['body'])
                                                                }
                                                                let tanggal_pembuatan = ''
                                                                if (typeof data_url[3] !== 'undefined') {
                                                                    tanggal_pembuatan = data_url[3]['body']
                                                                }
                                                                let keterangan = ''
                                                                if (typeof data_url[4] !== 'undefined') {
                                                                    keterangan = regex(data_url[4]['body'])
                                                                }
                                                                let instansi = ''
                                                                if (typeof data_url[6] !== 'undefined') {
                                                                    instansi = regex(data_url[6]['body'])
                                                                }
                                                                let satuan_kerja = ''
                                                                if (typeof data_url[7] !== 'undefined') {
                                                                    satuan_kerja = regex(data_url[7]['body'])
                                                                }
                                                                let kategori = ''
                                                                if (typeof data_url[8] !== 'undefined') {
                                                                    kategori = regex(data_url[8]['body'])
                                                                }
                                                                let sistem_pengadaan = ''
                                                                if (typeof data_url[9] !== 'undefined') {
                                                                    sistem_pengadaan = regex(data_url[9]['body'])
                                                                }
                                                                let tahun_anggaran = ''
                                                                if (typeof data_url[10] !== 'undefined') {
                                                                    tahun_anggaran = regex(data_url[10]['body'])
                                                                }
                                                                let nilai_pagu_paket = ''
                                                                let nilai_hps_paket = ''
                                                                if (typeof data_url[11] !== 'undefined') {
                                                                    nilai_pagu_paket = data_url[11]['body'][0]
                                                                    nilai_hps_paket = data_url[11]['body'][1]
                                                                }
                                                                let peserta_tender = ''
                                                                if (typeof data_url[14] !== 'undefined') {
                                                                    peserta_tender = regex(data_url[14]['body'])
                                                                }
                                                                let sql = `SELECT id FROM informasi_tender WHERE kode_tender=$1`
                                                                let id_informasi_tender_update = await client.query(sql, [kode_tender])
                                                                    .then((res) => {
                                                                        if (res.rows.length !== 0) {
                                                                            return res.rows[0]['id']
                                                                        } else {
                                                                            return 0
                                                                        }
                                                                    })
                                                                    .catch(() => {
                                                                        return 0
                                                                    })
                                                                if (id_informasi_tender_update !== 0) {
                                                                    // update
                                                                    let sql = `UPDATE informasi_tender SET uniq_string='${uniq_string_informasi_tender}', kode_tender='${kode_tender}', nama_tender='${nama_tender}', id_rup=${id_rup}, 
                                                                                                tanggal_buat='${tanggal_pembuatan}', keterangan='${keterangan}', instansi='${instansi}', satuan_kerja='${satuan_kerja}', 
                                                                                                kategori='${kategori}', sistem_pengadaan='${sistem_pengadaan}', tahun_anggaran='${tahun_anggaran}', nilai_pagu_paket='${nilai_pagu_paket}', 
                                                                                                nilai_hps_paket='${nilai_hps_paket}', id_jenis_kontrak=${id_jenis_kontrak}, peserta_tender='${peserta_tender}', id_eproc4=${id_eproc4}, updatedat=now()
                                                                                            WHERE id = ${id_informasi_tender_update}`
                                                                    let update_informasi_tender = await client.query(sql)
                                                                        .then(() => {
                                                                            return true
                                                                        })
                                                                        .catch(() => {
                                                                            return false
                                                                        })
                                                                    if (update_informasi_tender) {
                                                                        // lanjut
                                                                        let sql = `DELETE FROM jadwal WHERE id_informasi_tender=$1`
                                                                        param_update += await client.query(sql, [id_informasi_tender_update])
                                                                            .then(async () => {
                                                                                let param = 0
                                                                                let jadwal = ''
                                                                                let jadwal_url = ''
                                                                                if (typeof data_url[5] !== 'undefined') {
                                                                                    jadwal = data_url[5]['body']['jadwal']
                                                                                    jadwal_url = data_url[5]['body']['url']
                                                                                    if (typeof jadwal !== 'undefined') {
                                                                                        for (let j = 0; j < jadwal.length; j++) {
                                                                                            let no = regex(jadwal[j][0])
                                                                                            let tahap = regex(jadwal[j][1])
                                                                                            let mulai = regex(jadwal[j][2])
                                                                                            let sampai = regex(jadwal[j][3])
                                                                                            let perubahan = regex(jadwal[j][4])
                                                                                            let sql = "INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES(" + id_informasi_tender_update + ", '" + no + "', '" + tahap + "', '" + mulai + "', '" + sampai + "', '" + perubahan + "', '" + jadwal_url + "', now(), now())"
                                                                                            param += await client.query(sql)
                                                                                                .then(() => {
                                                                                                    return 1
                                                                                                })
                                                                                        }
                                                                                    }
                                                                                }
                                                                                return param
                                                                            })
                                                                    } else {
                                                                        // insert
                                                                        let sql_informasi_tender = `INSERT INTO informasi_tender(uniq_string, kode_tender, nama_tender, id_rup, tanggal_buat, keterangan, instansi, satuan_kerja, kategori, sistem_pengadaan, tahun_anggaran, nilai_pagu_paket, nilai_hps_paket, id_jenis_kontrak, peserta_tender, id_eproc4, createdat, updatedat) 
                                                                                        VALUES('${uniq_string_informasi_tender}', '${kode_tender}', '${nama_tender}', ${id_rup}, '${tanggal_pembuatan}', '${keterangan}', '${instansi}', '${satuan_kerja}', '${kategori}', '${sistem_pengadaan}', '${tahun_anggaran}', '${nilai_pagu_paket}', '${nilai_hps_paket}', ${id_jenis_kontrak}, '${peserta_tender}', ${id_eproc4}, now(), now())`
                                                                        let id_informasi_tender = await client.query(sql_informasi_tender)
                                                                            .then(async (res) => {
                                                                                return await client.query('SELECT id FROM informasi_tender WHERE kode_tender=$1 AND uniq_string=$2', [kode_tender, uniq_string_informasi_tender])
                                                                                    .then((r) => {
                                                                                        return r.rows[0]['id']
                                                                                    })
                                                                                    .catch((e) => {
                                                                                        return 0
                                                                                    })
                                                                            })
                                                                        if (id_informasi_tender !== 0) {
                                                                            let jadwal = ''
                                                                            let jadwal_url = ''
                                                                            if (typeof data_url[5] !== 'undefined') {
                                                                                jadwal = data_url[5]['body']['jadwal']
                                                                                jadwal_url = data_url[5]['body']['url']

                                                                                for (let j = 0; j < jadwal.length; j++) {
                                                                                    let no = regex(jadwal[j][0])
                                                                                    let tahap = regex(jadwal[j][1])
                                                                                    let mulai = regex(jadwal[j][2])
                                                                                    let sampai = regex(jadwal[j][3])
                                                                                    let perubahan = regex(jadwal[j][4])
                                                                                    let sql = "INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES('" + id_informasi_tender + "', '" + no + "', '" + tahap + "', '" + mulai + "', '" + sampai + "', '" + perubahan + "', '" + jadwal_url + "', now(), now())"
                                                                                    await client.query(sql)
                                                                                        .then(() => {
                                                                                            let success = 1
                                                                                            return success
                                                                                        })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    // insert
                                                                    let sql_informasi_tender = `INSERT INTO informasi_tender(uniq_string, kode_tender, nama_tender, id_rup, tanggal_buat, keterangan, instansi, satuan_kerja, kategori, sistem_pengadaan, tahun_anggaran, nilai_pagu_paket, nilai_hps_paket, id_jenis_kontrak, peserta_tender, id_eproc4, createdat, updatedat) 
                                                                                        VALUES('${uniq_string_informasi_tender}', '${kode_tender}', '${nama_tender}', ${id_rup}, '${tanggal_pembuatan}', '${keterangan}', '${instansi}', '${satuan_kerja}', '${kategori}', '${sistem_pengadaan}', '${tahun_anggaran}', '${nilai_pagu_paket}', '${nilai_hps_paket}', ${id_jenis_kontrak}, '${peserta_tender}', ${id_eproc4}, now(), now())`
                                                                    let id_informasi_tender = await client.query(sql_informasi_tender)
                                                                        .then(async (res) => {
                                                                            return await client.query('SELECT id FROM informasi_tender WHERE kode_tender=$1 AND uniq_string=$2', [kode_tender, uniq_string_informasi_tender])
                                                                                .then((r) => {
                                                                                    return r.rows[0]['id']
                                                                                })
                                                                                .catch((e) => {
                                                                                    return 0
                                                                                })
                                                                        })
                                                                    if (id_informasi_tender !== 0) {
                                                                        let jadwal = ''
                                                                        let jadwal_url = ''
                                                                        if (typeof data_url[5] !== 'undefined') {
                                                                            jadwal = data_url[5]['body']['jadwal']
                                                                            jadwal_url = data_url[5]['body']['url']

                                                                            if (typeof jadwal !== 'undefined') {
                                                                                for (let j = 0; j < jadwal.length; j++) {
                                                                                    let no = regex(jadwal[j][0])
                                                                                    let tahap = regex(jadwal[j][1])
                                                                                    let mulai = regex(jadwal[j][2])
                                                                                    let sampai = regex(jadwal[j][3])
                                                                                    let perubahan = regex(jadwal[j][4])
                                                                                    let sql = "INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES('" + id_informasi_tender + "', '" + no + "', '" + tahap + "', '" + mulai + "', '" + sampai + "', '" + perubahan + "', '" + jadwal_url + "', now(), now())"
                                                                                    await client.query(sql)
                                                                                        .then(() => {
                                                                                            let success = 1
                                                                                            return success
                                                                                        })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                let sql = "DELETE FROM jenis_kontrak WHERE uniq_string=$1"
                                                                client.query(sql, [uniq_string_jenis_kontrak])
                                                                console.log('ABORTED : Jenis Kontrak - Unique : ' + uniq_string_jenis_kontrak)
                                                            }
                                                        } else {
                                                            param_update = 0
                                                        }
                                                        return param_update
                                                    })
                                            }
                                        } else {
                                            insert_data_loop_tender++
                                            console.log('Status : Inserting data ' + insert_data_loop_tender + ' | Tender')
                                            // let id_lpse_fk = id
                                            let sql = "INSERT INTO eproc4(id_lpse, judul, hps, akhir_pendaftaran, url, uniq_string, jenis, createdat, updatedat) VALUES ('" + id + "', '" + judul + "', '" + hps + "', '" + akhir_pendaftaran + "', '" + url_pengumuman_lelang + "', '" + uniq_string + "', 'tender', now(), now())"
                                            param_end_connection += await client
                                                .query(sql)
                                                .then(async () => {
                                                    let param = 0
                                                    let kode_rup = ''
                                                    let nama_paket = ''
                                                    let sumber_dana = ''
                                                    if (typeof data_url[2]['body'] !== 'undefined') {
                                                        if (typeof data_url[2]['body']['Kode RUP'] !== 'undefined') {
                                                            kode_rup = regex(data_url[2]['body']['Kode RUP'])
                                                            nama_paket = regex(data_url[2]['body']['Nama Paket'])
                                                            sumber_dana = regex(data_url[2]['body']['Sumber Dana'])
                                                        }
                                                    }
                                                    let tanggal_pembuatan = ''
                                                    if (typeof data_url[3]['body'] !== 'undefined') {
                                                        tanggal_pembuatan = regex(data_url[3]['body'])

                                                    }
                                                    let keterangan = ''
                                                    if (typeof data_url[4]['body'] !== 'undefined') {
                                                        keterangan = regex(data_url[4]['body'])
                                                    }
                                                    let id_rup = await client
                                                        .query("INSERT INTO rup(kode_rup, nama_paket, sumber_dana, uniq_string, createdat, updatedat) VALUES('" + kode_rup + "', '" + nama_paket + "', '" + sumber_dana + "', '" + uniq_string_rup + "', now(), now())")
                                                        .then(async () => {
                                                            let id = client.query("SELECT id FROM rup WHERE uniq_string = '" + uniq_string_rup + "'")
                                                                .then((res) => {
                                                                    return res.rows[0]['id']
                                                                })
                                                            return id
                                                        })
                                                    let cara_pembayaran = ''
                                                    let lokasi_pekerjaan = ''
                                                    let kualifikasi_usaha = ''
                                                    if (typeof data_url[12]['body'] !== 'undefined') {
                                                        cara_pembayaran = regex(data_url[12]['body']['Cara Pembayaran'])
                                                        lokasi_pekerjaan = regex(data_url[12]['body']['Lokasi Pekerjaan'])
                                                        kualifikasi_usaha = regex(data_url[12]['body']['Kualifikasi Usaha'])
                                                    }
                                                    let id_jenis_kontrak = await client.query("INSERT INTO jenis_kontrak(cara_pembayaran, lokasi_pekerjaan, kualifikasi_usaha, uniq_string, createdat, updatedat) VALUES('" + cara_pembayaran + "', '" + lokasi_pekerjaan + "', '" + kualifikasi_usaha + "', '" + uniq_string_jenis_kontrak + "', now(), now())")
                                                        .then(async () => {
                                                            let id = await client.query("SELECT id FROM jenis_kontrak WHERE uniq_string = '" + uniq_string_jenis_kontrak + "'")
                                                                .then(async (result) => {
                                                                    return result.rows[0]['id']
                                                                })
                                                            return id
                                                        })
                                                    let kode_tender = ''
                                                    if (typeof data_url[0]['body'] !== 'undefined') {
                                                        kode_tender = regex(data_url[0]['body'])
                                                    }
                                                    let nama_tender = ''
                                                    if (typeof data_url[1]['body'] !== 'undefined') {
                                                        nama_tender = regex(data_url[1]['body'])
                                                    }
                                                    let instansi = ''
                                                    if (typeof data_url[6]['body'] !== 'undefined') {
                                                        instansi = regex(data_url[6]['body'])
                                                    }
                                                    let satuan_kerja = ''
                                                    if (typeof data_url[7]['body']) {
                                                        satuan_kerja = regex(data_url[7]['body'])
                                                    }
                                                    let kategori = ''
                                                    if (typeof data_url[8]['body'] !== 'undefined') {
                                                        kategori = regex(data_url[8]['body'])
                                                    }
                                                    let sistem_pengadaan = ''
                                                    if (typeof data_url[9]['body'] !== 'undefined') {
                                                        sistem_pengadaan = regex(data_url[9]['body'])
                                                    }
                                                    let tahun_anggaran = ''
                                                    if (typeof data_url[10]['body'] !== 'undefined') {
                                                        tahun_anggaran = data_url[10]['body']
                                                    }
                                                    let nilai_pagu_paket = ''
                                                    let nilai_hps_paket = ''
                                                    if (typeof data_url[11]['body'] !== 'undefined') {
                                                        nilai_pagu_paket = data_url[11]['body'][0]
                                                        nilai_hps_paket = data_url[11]['body'][1]
                                                    }
                                                    let peserta_tender = ''
                                                    if (typeof data_url[14]['body'] !== 'undefined') {
                                                        peserta_tender = regex(data_url[14]['body'])
                                                    }
                                                    let sql = `INSERT INTO informasi_tender(uniq_string, kode_tender, nama_tender, id_rup, tanggal_buat, keterangan, instansi, satuan_kerja, kategori, sistem_pengadaan, tahun_anggaran, nilai_pagu_paket, nilai_hps_paket, id_jenis_kontrak, peserta_tender, createdAt, updatedAt) 
                                                                VALUES('${uniq_string_informasi_tender}', '${kode_tender}', '${nama_tender}', ${id_rup}, '${tanggal_pembuatan}', '${keterangan}', '${instansi}', '${satuan_kerja}', '${kategori}', '${sistem_pengadaan}', '${tahun_anggaran}', '${nilai_pagu_paket}', '${nilai_hps_paket}', ${id_jenis_kontrak}, '${peserta_tender}', now(), now())`
                                                    let id_informasi_tender = await client.query(sql)
                                                        .then(async () => {
                                                            let id = await client.query("SELECT * FROM informasi_tender WHERE uniq_string = '" + uniq_string_informasi_tender + "'")
                                                                .then((res) => {
                                                                    return res.rows[0]['id']
                                                                })
                                                            return id
                                                        })

                                                    let jadwal = data_url[5]['body']['jadwal'] // array
                                                    let jadwal_url = data_url[5]['body']['url']
                                                    if (typeof jadwal != 'undefined') {
                                                        for (let j = 0; j < jadwal.length; j++) {
                                                            let no = regex(jadwal[j][0])
                                                            let tahap = regex(jadwal[j][1])
                                                            let mulai = regex(jadwal[j][2])
                                                            let sampai = regex(jadwal[j][3])
                                                            let perubahan = regex(jadwal[j][4])
                                                            let sql = "INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES(" + id_informasi_tender + ", '" + no + "', '" + tahap + "', '" + mulai + "', '" + sampai + "', '" + perubahan + "', '" + jadwal_url + "', now(). now())"
                                                            param += await client.query(sql)
                                                                .then(() => {
                                                                    let success = 1
                                                                    return success
                                                                })
                                                        }
                                                    }
                                                    return param
                                                })
                                                .catch(e => console.error(e.stack))
                                        }
                                    }
                                } else {
                                    client.end()
                                }
                            }
                        }
                    }
                } else {
                    param_end_connection = 1
                }
            } else {
                param_end_connection = 1
            }

            let non_tender = arr[a]
            if (typeof non_tender != 'undefined') {
                let url = non_tender['url']
                non_tender = non_tender['non_tender']
                // NON TENDER
                if (non_tender.length != 0) {
                    for (let x = 0; x < non_tender.length; x++) {
                        let data = non_tender[x]['data']
                        for (let a = 0; a < data.length; a++) {
                            let judul = regex(non_tender[x]['data'][a]['judul'])
                            let hps = regex(non_tender[x]['data'][a]['hps'])
                            let akhir_pendaftaran = regex(non_tender[x]['data'][a]['akhir_pendaftaran'])
                            let url_pengumuman_lelang = non_tender[x]['data'][a]['url']
                            let data_url = non_tender[x]['data'][a]['data_url']

                            let id_lpse = await client
                                .query("SELECT id FROM list_lpse WHERE website like '%" + url + "%'")
                                .then(res => {
                                    return res.rows
                                })
                                .catch(e => console.error(e.stack))
                            if (id_lpse.length !== 0 || id_lpse !== null) {
                                for (let i = 0; i < id_lpse.length; i++) {
                                    // generate random string
                                    let uniq_string = rs.generate(40)
                                    let uniq_string_informasi_tender = rs.generate(40)
                                    let uniq_string_jenis_kontrak = rs.generate(40)

                                    let id = id_lpse[i]['id']
                                    let is_exist = false;
                                    is_exist = await client
                                        .query(`SELECT count(*) is_exist FROM eproc4 WHERE judul='${judul}' AND hps='${hps}' AND akhir_pendaftaran='${akhir_pendaftaran}' AND id_lpse='${id}'`)
                                        .then((res) => {
                                            if (res.rows[0]['is_exist'] === '0') {
                                                return false
                                            } else {
                                                return true
                                            }
                                        })
                                    if (is_exist) {
                                        update_data_loop_non_tender++
                                        console.log('Status : Updating data ' + update_data_loop_non_tender + ' | Non Tender')
                                        let uniq_string_eproc4 = await client
                                            .query(`SELECT uniq_string FROM eproc4 WHERE judul='${judul}' AND hps='${hps}' AND akhir_pendaftaran='${akhir_pendaftaran}' AND id_lpse='${id}'`)
                                            .then((res) => {
                                                return res.rows[0]['uniq_string']
                                            })
                                        let sql = "UPDATE eproc4 SET judul='" + judul + "', hps='" + hps + "', akhir_pendaftaran='" + akhir_pendaftaran + "', updatedat=now() WHERE id_lpse=" + id + " AND uniq_string='" + uniq_string_eproc4 + "'"
                                        param_end_connection += await client
                                            .query(sql)
                                            .then(async () => {
                                                if (typeof data_url !== 'undefined') {
                                                    let param_update = 0
                                                    // start param - jenis kontrak
                                                    let lokasi_pekerjaan = ''
                                                    if (typeof data_url[11] !== 'undefined') {
                                                        lokasi_pekerjaan = regex(data_url[11]['body'])
                                                    }
                                                    let kualifikasi_usaha = ''
                                                    if (typeof data_url[12] !== 'undefined') {
                                                        kualifikasi_usaha = regex(data_url[12]['body'])
                                                    }
                                                    // end param - jenis kontrak
                                                    let sql_jenis_kontrak = "SELECT id FROM jenis_kontrak WHERE lokasi_pekerjaan=$1 AND kualifikasi_usaha=$2"
                                                    let arr_jenis_kontrak = [
                                                        lokasi_pekerjaan,
                                                        kualifikasi_usaha
                                                    ]
                                                    let cek_jenis_kontrak = await client.query(sql_jenis_kontrak, arr_jenis_kontrak)
                                                        .then(async (res) => {
                                                            if (res.rows.length === 0) {
                                                                let sql_insert_jenis_kontrak = `INSERT INTO jenis_kontrak(cara_pembayaran, lokasi_pekerjaan, kualifikasi_usaha, uniq_string, createdat, updatedat) VALUES (null, '${lokasi_pekerjaan}', '${kualifikasi_usaha}', '${uniq_string_jenis_kontrak}', now(), now())`
                                                                let insert_jenis_kontrak = await client.query(sql_insert_jenis_kontrak)
                                                                    .then(() => {
                                                                        return 'berhasil'
                                                                    })
                                                                    .catch((e) => {
                                                                        return 'gagal'
                                                                    })
                                                                return insert_jenis_kontrak
                                                            } else {
                                                                let id = res.rows[0]['id']
                                                                let sql_update_jenis_kontrak = `UPDATE jenis_kontrak SET lokasi_pekerjaan='${lokasi_pekerjaan}', kualifikasi_usaha='${kualifikasi_usaha}', uniq_string='${uniq_string_jenis_kontrak}', updatedat=now() WHERE id=${id}`
                                                                let update_jenis_kontrak = await client.query(sql_update_jenis_kontrak)
                                                                    .then(() => {
                                                                        return 'berhasil'
                                                                    })
                                                                    .catch((e) => {
                                                                        return 'gagal'
                                                                    })
                                                                return update_jenis_kontrak
                                                            }
                                                        })
                                                    if (cek_jenis_kontrak == 'berhasil') {
                                                        let sql_get_id_jenis_kontrak = `SELECT id FROM jenis_kontrak WHERE uniq_string='${uniq_string_jenis_kontrak}'`
                                                        let id_jenis_kontrak = await client.query(sql_get_id_jenis_kontrak)
                                                            .then((r) => {
                                                                return r.rows[0]['id']
                                                            })
                                                            .catch((e) => {
                                                                return 0
                                                            })
                                                        if (id_jenis_kontrak !== 0) {
                                                            let kode_paket = ''
                                                            if (typeof data_url[0] !== 'undefined') {
                                                                kode_paket = regex(data_url[0]['body'])
                                                            }
                                                            let nama_paket = ''
                                                            if (typeof data_url[1] !== 'undefined') {
                                                                nama_paket = regex(data_url[1]['body'])
                                                            }
                                                            let tanggal_pembuatan = ''
                                                            if (typeof data_url[2] !== 'undefined') {
                                                                tanggal_pembuatan = data_url[2]['body']
                                                            }
                                                            let keterangan = ''
                                                            if (typeof data_url[3] !== 'undefined') {
                                                                keterangan = regex(data_url[3]['body'])
                                                            }
                                                            let instansi = ''
                                                            if (typeof data_url[5] !== 'undefined') {
                                                                instansi = regex(data_url[5]['body'])
                                                            }
                                                            let satuan_kerja = ''
                                                            if (typeof data_url[6] !== 'undefined') {
                                                                satuan_kerja = regex(data_url[6]['body'])
                                                            }
                                                            let kategori = ''
                                                            if (typeof data_url[7] !== 'undefined') {
                                                                kategori = regex(data_url[7]['body'])
                                                            }
                                                            let metode_pengadaan = ''
                                                            if (typeof data_url[8] !== 'undefined') {
                                                                metode_pengadaan = regex(data_url[8]['body'])
                                                            }
                                                            let tahun_anggaran = ''
                                                            if (typeof data_url[9] !== 'undefined') {
                                                                tahun_anggaran = regex(data_url[9]['body'])
                                                            }
                                                            let nilai_pagu_paket = ''
                                                            let nilai_hps_paket = ''
                                                            if (typeof data_url[10] !== 'undefined') {
                                                                nilai_pagu_paket = data_url[10]['body'][0]
                                                                nilai_hps_paket = data_url[10]['body'][1]
                                                            }
                                                            let id_eproc4 = await client.query(`SELECT id FROM eproc4 WHERE uniq_string='${uniq_string_eproc4}'`)
                                                                .then((r) => {
                                                                    return r.rows[0]['id']
                                                                })
                                                            let sql_cek = `SELECT id 
                                                                FROM informasi_tender 
                                                                WHERE kode_tender=$1 AND nama_tender=$2 AND tanggal_buat=$3 
                                                                AND keterangan=$4 AND instansi=$5 AND satuan_kerja=$6 
                                                                AND kategori=$7 AND sistem_pengadaan=$8 AND tahun_anggaran=$9 
                                                                AND nilai_pagu_paket=$10 AND nilai_hps_paket=$11 AND id_jenis_kontrak=$12
                                                                AND id_eproc4=$13`
                                                            let arr_cek = [
                                                                kode_paket,
                                                                nama_paket,
                                                                tanggal_pembuatan,
                                                                keterangan,
                                                                instansi,
                                                                satuan_kerja,
                                                                kategori,
                                                                metode_pengadaan,
                                                                tahun_anggaran,
                                                                nilai_pagu_paket,
                                                                nilai_hps_paket,
                                                                id_jenis_kontrak,
                                                                id_eproc4,
                                                            ]
                                                            let cek_informasi_tender = await client.query(sql_cek, arr_cek)
                                                                .then((r) => {
                                                                    return r.rows
                                                                })
                                                            if (cek_informasi_tender.length !== 0) {
                                                                let id_informasi_tender = cek_informasi_tender[0]['id']
                                                                let sql_update_informasi_tender = `UPDATE informasi_tender SET kode_tender='${kode_paket}', nama_tender='${nama_paket}', tanggal_buat='${tanggal_pembuatan}', 
                                                                                                    keterangan='${keterangan}', instansi='${instansi}', satuan_kerja='${satuan_kerja}', 
                                                                                                    kategori='${kategori}', sistem_pengadaan='${metode_pengadaan}', tahun_anggaran='${tahun_anggaran}', 
                                                                                                    nilai_pagu_paket='${nilai_pagu_paket}', nilai_hps_paket='${nilai_hps_paket}', id_jenis_kontrak=${id_jenis_kontrak},
                                                                                                    id_eproc4=${id_eproc4}, updatedat=now()
                                                                                                    WHERE id=${id_informasi_tender}`
                                                                let update_informasi_tender = await client.query(sql_update_informasi_tender)
                                                                    .then((r) => {
                                                                        return 'berhasil'
                                                                    })
                                                                    .catch((e) => {
                                                                        return 'gagal'
                                                                    })
                                                                if (update_informasi_tender == 'berhasil') {
                                                                    // cek jadwal
                                                                    let sql_cek_jadwal = `SELECT id FROM jadwal WHERE id_informasi_tender=$1`
                                                                    let cek_jadwal = await client.query(sql_cek_jadwal, [id_informasi_tender])
                                                                        .then((r) => {
                                                                            return r.rows
                                                                        })
                                                                    if (cek_jadwal.length != 0) {
                                                                        let jadwal = data_url[4]['body']['jadwal']
                                                                        let jadwal_url = data_url[4]['body']['url']
                                                                        param_update += await client.query(`DELETE FROM jadwal WHERE id_informasi_tender=$1`, [id_informasi_tender])
                                                                            .then(async () => {
                                                                                let param = 0
                                                                                for (let i = 0; i < jadwal.length; i++) {
                                                                                    let nomor = regex(jadwal[i][0])
                                                                                    let tahap = regex(jadwal[i][1])
                                                                                    let mulai = regex(jadwal[i][2])
                                                                                    let sampai = regex(jadwal[i][3])
                                                                                    let perubahan = regex(jadwal[i][4])
                                                                                    let sql_insert_jadwal = `INSERT INTO jadwal(tahap, mulai, sampai, perubahan, nomor, id_informasi_tender, url, createdat, updatedat) VALUES('${tahap}', '${mulai}', '${sampai}', '${perubahan}', '${nomor}', ${id_informasi_tender}, '${jadwal_url}', now(), now())`
                                                                                    param += await client.query(sql_insert_jadwal)
                                                                                        .then(() => {
                                                                                            return 1
                                                                                        })
                                                                                }
                                                                                return param
                                                                            })
                                                                    } else {
                                                                        let jadwal = data_url[4]['body']['jadwal']
                                                                        for (let i = 0; i < jadwal.length; i++) {
                                                                            let nomor = regex(jadwal[i][0])
                                                                            let tahap = regex(jadwal[i][1])
                                                                            let mulai = regex(jadwal[i][2])
                                                                            let sampai = regex(jadwal[i][3])
                                                                            let perubahan = regex(jadwal[i][4])
                                                                            let sql_insert_jadwal = `INSERT INTO jadwal(tahap, mulai, sampai, perubahan, nomor, id_informasi_tender, createdat, updatedat) VALUES('${tahap}', '${mulai}', '${sampai}', '${perubahan}', '${nomor}', ${id_informasi_tender}, now(), now())`
                                                                            param_update += await client.query(sql_insert_jadwal)
                                                                                .then(() => {
                                                                                    return 1
                                                                                })
                                                                        }
                                                                    }
                                                                } else {
                                                                    param_update = 0
                                                                }
                                                            } else {
                                                                let sql = `INSERT INTO informasi_tender(uniq_string, kode_tender, nama_tender, tanggal_buat, keterangan, instansi, satuan_kerja, kategori, sistem_pengadaan, tahun_anggaran, nilai_pagu_paket, nilai_hps_paket, id_jenis_kontrak, id_eproc4, createdat, updatedat) 
                                                                            VALUES('${uniq_string_informasi_tender}', '${kode_paket}', '${nama_paket}', '${tanggal_pembuatan}', '${keterangan}', '${instansi}', '${satuan_kerja}', '${kategori}', '${metode_pengadaan}', '${tahun_anggaran}', '${nilai_pagu_paket}', '${nilai_hps_paket}', ${id_jenis_kontrak}, '${id_eproc4}', now(), now())`
                                                                let id_informasi_tender = await client.query(sql)
                                                                    .then(async () => {
                                                                        let id = await client.query("SELECT * FROM informasi_tender WHERE uniq_string = '" + uniq_string_informasi_tender + "'")
                                                                            .then((res) => {
                                                                                return res.rows[0]['id']
                                                                            })
                                                                        return id
                                                                    })
                                                                let jadwal = data_url[4]['body']['jadwal'] // array
                                                                let jadwal_url = data_url[4]['body']['url']
                                                                if (typeof jadwal != 'undefined') {
                                                                    for (let j = 0; j < jadwal.length; j++) {
                                                                        let no = regex(jadwal[j][0])
                                                                        let tahap = regex(jadwal[j][1])
                                                                        let mulai = regex(jadwal[j][2])
                                                                        let sampai = regex(jadwal[j][3])
                                                                        let perubahan = regex(jadwal[j][4])
                                                                        let sql = `INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES(${id_informasi_tender}, '${no}', '${tahap}', '${mulai}', '${sampai}', '${perubahan}', '${jadwal_url}', now(), now())`
                                                                        param_update += await client.query(sql)
                                                                            .then(() => {
                                                                                let success = 1
                                                                                return success
                                                                            })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        param_update = 0
                                                    }
                                                    return param_update
                                                }
                                            })
                                    } else {
                                        insert_data_loop_non_tender++
                                        console.log('Status : Inserting data ' + insert_data_loop_non_tender + ' | Non Tender')
                                        let id_lpse_fk = id
                                        let sql = `INSERT INTO eproc4(id_lpse, judul, hps, akhir_pendaftaran, url, uniq_string, jenis, createdat, updatedat) VALUES (${id_lpse_fk}, '${judul}', '${hps}', '${akhir_pendaftaran}', '${url_pengumuman_lelang}', '${uniq_string}', 'non_tender', now(), now())`
                                        param_end_connection += await client
                                            .query(sql)
                                            .then(async (res) => {
                                                let id_eproc4 = await client.query(`SELECT id FROM eproc4 WHERE uniq_string='${uniq_string}'`)
                                                    .then((r) => {
                                                        return r.rows[0]['id']
                                                    })
                                                let param = 0
                                                let kode_paket = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[0] !== 'undefined') {
                                                        kode_paket = regex(data_url[0]['body'])
                                                    }
                                                }
                                                let nama_paket = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[1] !== 'undefined') {
                                                        nama_paket = regex(data_url[1]['body'])
                                                    }
                                                }
                                                let tanggal_pembuatan = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[2] !== 'undefined') {
                                                        tanggal_pembuatan = data_url[2]['body']
                                                    }
                                                }
                                                let keterangan = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[3] !== 'undefined') {
                                                        keterangan = regex(data_url[3]['body'])
                                                    }
                                                }
                                                let instansi = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[5] !== 'undefined') {
                                                        instansi = regex(data_url[5]['body'])
                                                    }
                                                }
                                                let satuan_kerja = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[6] !== 'undefined') {
                                                        satuan_kerja = regex(data_url[6]['body'])
                                                    }
                                                }
                                                let kategori = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[7] !== 'undefined') {
                                                        kategori = regex(data_url[7]['body'])
                                                    }
                                                }
                                                let metode_pengadaan = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[8] !== 'undefined') {
                                                        metode_pengadaan = regex(data_url[8]['body'])
                                                    }
                                                }
                                                let tahun_anggaran = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[9] !== 'undefined') {
                                                        tahun_anggaran = data_url[9]['body']
                                                    }
                                                }
                                                let nilai_pagu_paket = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[10] !== 'undefined') {
                                                        nilai_pagu_paket = regex(data_url[10]['body'][0])
                                                    }
                                                }
                                                let nilai_hps_paket = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[10] !== 'undefined') {
                                                        nilai_hps_paket = regex(data_url[10]['body'][1])
                                                    }
                                                }
                                                let lokasi_pekerjaan = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[11] !== 'undefined') {
                                                        lokasi_pekerjaan = regex(data_url[11]['body'])
                                                    }
                                                }
                                                let kualifikasi_usaha = ''
                                                if (typeof data_url !== 'undefined') {
                                                    if (typeof data_url[12] !== 'undefined') {
                                                        kualifikasi_usaha = regex(data_url[12]['body'])
                                                    }
                                                }
                                                let id_jenis_kontrak = await client.query("INSERT INTO jenis_kontrak(cara_pembayaran, lokasi_pekerjaan, kualifikasi_usaha, uniq_string, createdat, updatedat) VALUES(null, '" + lokasi_pekerjaan + "', '" + kualifikasi_usaha + "', '" + uniq_string_jenis_kontrak + "', now(), now())")
                                                    .then(async () => {
                                                        let id = await client.query("SELECT id FROM jenis_kontrak WHERE uniq_string = '" + uniq_string_jenis_kontrak + "'")
                                                            .then(async (result) => {
                                                                return result.rows[0]['id']
                                                            })
                                                        return id
                                                    })
                                                let sql = `INSERT INTO informasi_tender(uniq_string, kode_tender, nama_tender, tanggal_buat, keterangan, instansi, satuan_kerja, kategori, sistem_pengadaan, tahun_anggaran, nilai_pagu_paket, nilai_hps_paket, id_jenis_kontrak, id_eproc4, createdat, updatedat) 
                                                        VALUES('${uniq_string_informasi_tender}', '${kode_paket}', '${nama_paket}', '${tanggal_pembuatan}', '${keterangan}', '${instansi}', '${satuan_kerja}', '${kategori}', '${metode_pengadaan}', '${tahun_anggaran}', '${nilai_pagu_paket}', '${nilai_hps_paket}', ${id_jenis_kontrak}, '${id_eproc4}', now(), now())`
                                                let id_informasi_tender = await client.query(sql)
                                                    .then(async () => {
                                                        let id = await client.query("SELECT * FROM informasi_tender WHERE uniq_string = '" + uniq_string_informasi_tender + "'")
                                                            .then((res) => {
                                                                return res.rows[0]['id']
                                                            })
                                                        return id
                                                    })
                                                if (typeof data_url[4] != 'undefined') {
                                                    if (typeof data_url[4]['body'] != 'undefined') {
                                                        let jadwal = data_url[4]['body']['jadwal'] // array
                                                        let jadwal_url = data_url[4]['body']['url']
                                                        if (typeof jadwal !== 'undefined') {
                                                            for (let j = 0; j < jadwal.length; j++) {
                                                                let no = regex(jadwal[j][0])
                                                                let tahap = regex(jadwal[j][1])
                                                                let mulai = regex(jadwal[j][2])
                                                                let sampai = regex(jadwal[j][3])
                                                                let perubahan = regex(jadwal[j][4])
                                                                let sql = `INSERT INTO jadwal(id_informasi_tender, nomor, tahap, mulai, sampai, perubahan, url, createdat, updatedat) VALUES(${id_informasi_tender}, '${no}', '${tahap}', '${mulai}', '${sampai}', '${perubahan}', '${jadwal_url}', now(), now())`
                                                                param += await client.query(sql)
                                                                    .then(() => {
                                                                        let success = 1
                                                                        return success
                                                                    })
                                                            }
                                                        }
                                                    }
                                                    return param
                                                }
                                            })
                                            .catch(e => {
                                                console.error("==> " + e.stack)
                                            })
                                    }
                                }
                            } else {
                                client.end()
                            }
                        }
                    }
                } else {
                    param_end_connection = 1
                }
            } else {
                param_end_connection = 1
            }
        }
    } else {
        param_end_connection = 1
    }
    if (param_end_connection > 0) {
        if (insert_data_loop_non_tender != 0) {
            console.log("Status : " + insert_data_loop_non_tender + " non tender data inserted.")
        }
        if (update_data_loop_non_tender != 0) {
            console.log("Status : " + update_data_loop_non_tender + " non tender data updated.")
        }
        if (insert_data_loop_tender != 0) {
            console.log("Status : " + insert_data_loop_tender + " tender data inserted.")
        }
        if (update_data_loop_tender != 0) {
            console.log("Status : " + update_data_loop_tender + " tender data updated.")
        }
        clearInterval(inserting_interval)
        console.log('Status : Inserting and/or updating data took ' + functions.time_conversion(spend_time))

        setTimeout(() => {
                client.end()
                    .then(() => {
                        console.log('Status : Disconnected from database.')
                        console.log('Status : Done.')
                    })
                    .catch(e => console.error(e.stack))
            },
            1000)
    }
}

function regex(str) {
    return str.replace(/[^\w\s]/gi, '')
}

exports.insert = insert