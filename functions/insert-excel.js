const Excel = require('read-excel-file/node')
const {
    Client
} = require('pg')
const client = new Client({
    user: 'postgres',
    host: '127.0.0.1',
    database: 'lpse-web-scraping',
    password: 'firmantria10',
    post: 5432,
})

client.connect()
execute()

function load_data_excel() {
    const path = '../dokumen/list_lpse.xlsx'
    return Excel(path)
        .then(async function (rows) {
            let sql_insert = []
            let i = 0
            rows.forEach(column => {
                let nama_lpse = column[2]
                let website = column[3]
                let alamat = ''
                if (column[4] != null) {
                    alamat = column[4].split('\n').join(' ')
                } else {
                    alamat = ''
                }
                let temp = []
                temp['nama'] = nama_lpse
                temp['website'] = website
                temp['alamat'] = alamat
                sql_insert[i] = temp
                i++
            })
            sql_insert.splice(0, 1)
            return sql_insert
        })
}

async function execute() {
    let records = await load_data_excel()
    for (let index = 0; index < records.length; index++) {
        let sql = 'INSERT INTO list_lpse(nama_lpse, website, alamat, createdat, updatedat) VALUES ($1::text, $2::text, $3::text, now(), now())'
        client.query(sql, [records[index]['nama'], records[index]['website'], records[index]['alamat']], (error, result) => {
            if (!error) {
                console.log('Record ' + (index + 1) + ' inserted')
                if (index == records.length - 1) {
                    client.end()
                }
            } else {
                console.log('error : ' + error)
            }
        })
    }
}