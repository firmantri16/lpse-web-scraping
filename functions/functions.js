function time_conversion(sec_num){
    var hours   = Math.floor(sec_num / 3600)
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60)
    var seconds = sec_num - (hours * 3600) - (minutes * 60)

    return hours+' hours '+minutes+' minutes and '+seconds+' seconds'
}

exports.time_conversion = time_conversion
