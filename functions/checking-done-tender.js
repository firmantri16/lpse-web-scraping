require('dotenv').config({
    path: '../.env'
})
const {
    Client
} = require('pg')
const {
    disconnect
} = require('process')
const client = new Client({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
})
client.connect()
let paramDisconnect = 0
let countData = 0
async function check() {
    await client
        .query("SELECT * FROM eproc4 WHERE isdone != 1")
        .then(async res => {
            let now = Date().toString()
            let nowArr = now.split(" ")
            let time = convertTime(nowArr[4])
            let dateNow = nowArr[2] + " " + convertMonthToIndo(nowArr[1]) + " " + nowArr[3] + " " + time
            // console.log(dateNow)
            countData = res.rows.length
            if (paramDisconnect != countData) {
                for (let i = 0; i < res.rows.length; i++) {
                    let id_eproc4 = res.rows[i]['id']
                    if (Date.parse(formatDate(res.rows[i]['akhir_pendaftaran'])) <= Date.parse(formatDate(dateNow))) {
                        // update data yang selesai
                        await client
                            .query(`UPDATE eproc4 SET isdone = 1 WHERE id = ${id_eproc4}`)
                            .then(r => {
                                console.log('Status : Eproc4 ' + res.rows[i]['judul'] + " is done.")
                                paramDisconnect += 1
                            })
                    } else {
                        paramDisconnect += 1
                    }
                    if(paramDisconnect == countData){
                        console.log('Status : Checking is done.')
                        client.end()
                    }
                }
            }else{
                console.log('Status : Checking is done.')
                client.end()
            }
        })
        .catch(e => console.error(e.stack))
}

function formatDate(date) {
    date = date.split(" ")
    return convertMonthToEnglish(date[1]) + " " + date[0] + ", " + date[2]
}

function convertMonthToIndo(month) {
    switch (month) {
        case 'Jan':
            month = 'Januari'
            break;
        case 'Feb':
            month = 'Februari'
            break;
        case 'Mar':
            month = 'Maret'
            break;
        case 'Apr':
            month = 'April'
            break;
        case 'Mei':
            month = 'Mei'
            break;
        case 'Jun':
            month = 'Juni'
            break;
        case 'June':
            month = 'Juni'
            break;
        case 'Jul':
            month = 'Juli'
            break;
        case 'July':
            month = 'Juli'
            break;
        case 'Aug':
            month = 'Agustus'
            break;
        case 'Sep':
            month = 'September'
            break;
        case 'Oct':
            month = 'Oktober'
            break;
        case 'Nov':
            month = 'November'
            break;
        case 'Dec':
            month = 'Desember'
            break;
    }
    return month
}

function convertMonthToEnglish(month) {
    switch (month) {
        case 'Januari':
            month = 'January'
            break;
        case 'Februari':
            month = 'February'
            break;
        case 'Maret':
            month = 'March'
            break;
        case 'April':
            month = 'April'
            break;
        case 'Mei':
            month = 'May'
            break;
        case 'Juni':
            month = 'June'
            break;
        case 'Juli':
            month = 'July'
            break;
        case 'Agustus':
            month = 'August'
            break;
        case 'September':
            month = 'September'
            break;
        case 'Oktober':
            month = 'October'
            break;
        case 'November':
            month = 'November'
            break;
        case 'Desember':
            month = 'December'
            break;
    }
    return month
}

function convertTime(time) {
    time = time.split(":")
    return time[0] + "" + time[1]
}

exports.check = check